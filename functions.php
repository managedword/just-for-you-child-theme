<?php

class JustForYou {

	function __construct () {

		add_action( 'init', array( $this, 'enqueue_init' ) );
		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
		add_action( 'template_redirect', array( $this, 'add_products_to_cart' ) );

		add_filter( 'wc_empty_cart_message', array( $this, 'empty_cart_message' ) );

		add_shortcode ( 'display-products', array( $this, 'display_products_shortcode' ) );
		add_shortcode ( 'menu-cart', array( $this, 'display_menu_cart' ) );

	}

	function enqueue_init () {
		
	}

	function enqueue_scripts () {

		wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
		wp_enqueue_style( 'my-managed-word-style', get_stylesheet_directory_uri() . '/style.css' );
		wp_enqueue_script( 'my-managed-word-bundle', get_stylesheet_directory_uri() . '/dist/bundle.js', array( 'jquery' ), 1, false );
		
	}

	function empty_cart_message() {

		ob_start();
	
		include('partials/cart-empty-message.php');
	
		$html = ob_get_contents();
		ob_end_clean();
	
		return $html;

	}

	function display_menu_cart ( $atts, $content = null ) {

		ob_start();
	
		include('partials/cart-menu.php');
	
		$html = ob_get_contents();
		ob_end_clean();
	
		return $html;

	}

	function display_products_shortcode ( $atts, $content = null ) {

		$category = $atts['category'];

		// Grab all of the available products in a category
		$category_products = wc_get_products( [
			'category' => [ $category ]
		] );

		// We don't have anything, so quit
		if ( ! $category_products ) return;

		$products = [];

		foreach ( $category_products as $category_product ) {

			$product = [];
			$product['details'] = $category_product->get_data();
			$product['thumbnail'] = get_the_post_thumbnail_url( $product['details']['id'] );

			// Get all of the variations
			$variations = $category_product->get_available_variations();

			// Grab the price column and sort by that
			$sort_column = array_column( $variations, 'display_price' );
			array_multisort( $sort_column, SORT_ASC, $variations );
			$product['variations'] = $variations;

			$products[] = $product;

		}

		ob_start();
	
		include('partials/shortcode-product.php');
	
		$html = ob_get_contents();
		ob_end_clean();
	
		return $html;

	}

	function add_products_to_cart () {

		$categories = get_categories( array(
			'taxonomy' => 'product_cat',
			'orderby' => 'name',
		) );

		$added_to_cart = false;

		foreach( $categories as $category ) {

			$slug = $category->slug;

			if ( isset( $_POST['category-' . $slug] ) ) {
	
				$form_data = $_POST['category-' . $slug];
	
				foreach ( $form_data as $item => $quantity ) {

					if ( is_numeric( $quantity ) ) {
						$item_details = explode( '|', $item );
						$variation = wc_get_product( $item_details[1] );
						$attributes = $variation->get_data()['attributes'];

						WC()->cart->add_to_cart( $item_details[0], $quantity, $item_details[1], $attributes );
						$added_to_cart = true;
					}

				}
	
			}

		}

		if ( $added_to_cart ) {

			wp_redirect( '/cart/' );
			exit;

		}

	}

}

$myChildTheme = new JustForYou();