'use strict';

export default class ProductSelector {

	static bind ( category ) {

		var $selector = jQuery('#selector-' + category)
		var $widget = jQuery('#widget-' + category)

		$selector.bind('change', function ( event ) {

			var new_product = event.target.value

			console.log( 'Showing: ' + new_product )

			$widget.find('.product').hide()
			$widget.find( '#' + new_product ).show()

		})
	}

}

window.ProductSelector = ProductSelector