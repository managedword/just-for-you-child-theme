<div class="cart-empty-message" style="text-align: center;">
	<h3>Discover Our Collections</h3>
	<p>It looks like you haven't selected any packages yet. Visit our collections today and see what we have to offer.</p>
	<br>
	<a href="/collections/" class="button">See Our Collections</a>
</div>
