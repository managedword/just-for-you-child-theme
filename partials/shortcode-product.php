<?php

?>
<div class="display_products_widget" id="widget-<?php echo $category; ?>">
	<form class="form_variations" id="form-<?php echo $category; ?>" action="<?php echo get_permalink(); ?>" method="POST">
		<div class="product_selector">
				<select id="selector-<?php echo $category; ?>">
					<?php foreach ( $products as $product ) : ?>
						<option value="<?php echo $product['details']['slug']; ?>"><?php echo $product['details']['name']; ?></option>
					<?php endforeach; ?>
				</select>
		</div>
		<div class="products">
			<?php foreach ( $products as $product ) : ?>
				<div class="product" id="<?php echo $product['details']['slug']; ?>">
				<div class="product_details">
					<div class="product_image">
						<img src="<?php echo $product['thumbnail']; ?>" alt="">
					</div>
					<h5 class="product_name"><?php echo $product['details']['name']; ?></h5>
					<div class="product_description">
						<?php echo $product['details']['description']; ?>
					</div>
				</div>
					<table class="product_variations">
						<thead>
							<tr>
								<th class="option">Option</th>
								<th class="price">Price</th>
								<th class="quantity">Quantity</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ( $product['variations'] as $variation ) : ?>
								<tr>
									<td class="option"><?php echo $variation['attributes']['attribute_option']; ?></td>
									<td class="price"><?php echo $variation['price_html']; ?></td>
									<td class="quantity"><input type="text" class="input-text" id="<?php echo $variation['variation_id']; ?>" name="category-<?php echo $category; ?>[<?php echo $product['details']['id']; ?>|<?php echo $variation['variation_id']; ?>]" value="" /></td>
								</tr>
							<?php endforeach; ?>
						</tbody>
					</table>
					<button type="submit" class="submit_variation">Add to Cart</button>
				</div>
			<?php endforeach; ?>
		</div>
	</form>
</div>
<script>
	ProductSelector.bind( '<?php echo $category; ?>' )
</script>
