<?php
$cart_count = ( WC()->cart->get_cart_contents_count() > 0 ) ? '(' . WC()->cart->get_cart_contents_count() . ')' : '';
?>
<a class="cart-contents" href="<?php echo wc_get_cart_url(); ?>" title="<?php _e( 'View your shopping cart' ); ?>">
	Cart <?php echo $cart_count; ?>
</a>
